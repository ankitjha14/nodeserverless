const expect = require('chai').expect;
const sinon = require('sinon');



const scanService = require("../services/scanService");
const createService = require("../services/createService");
const updateService = require("../services/updateService");

let create = require("../api/create");

describe('createUser', function createUserTest() {
  let scanUserStub;
  let createUserStub;
  let updateUserStub;

  context('user sccessfully created', function () {
    let userParameter = {body:"{ \"fullname\": \"Ankit\", \"email\": \"ankitjha14@gmail.com\", \"mobile\": \"8588984781\", \"address\": \"pune\", \"company\": \"telstra\", \"title\": \"It\"  }"
  };
    let result = "result stub";

    before(function beforeTest() {

      scanUserStub = sinon.stub(scanService, "scanUser");
      scanUserStub.callsFake(function (body) {
        expect(body["fullname"]).to.eq("Ankit");

        return Promise.resolve(result);
      });

      createUserStub = sinon.stub(createService, "createUser");
      createUserStub.callsFake(function (body) {
        expect(body["fullname"]).to.eq("Ankit");

        return Promise.resolve(result);
      });
    });

    it('success', async function () {
      let event =  userParameter ;
      let context = {};

      let response = await create.create(event, context);

      expect(response.statusCode).to.eq(200);
      expect(response.body).to.eq('user sccessfully created');
    });

    after(function afterTest() {
      scanUserStub.restore();
       createUserStub.restore();
    });
  });

  context('invalid data', function () {
    let userParameter = {body:"{\"name\":\"some param\"}"};;
    it('failure', async function () {
      let event =  userParameter ;
      let context = {};

      let response = await create.create(event, context);

      expect(response.statusCode).to.eq(400);
      expect(response.body).to.eq('Invalid data');
    });
  });

  context('user sccessfully updated', function () {
    let userParameter = {body:"{ \"fullname\": \"Ankit\", \"email\": \"ankitjha14@gmail.com\", \"mobile\": \"8588984781\", \"address\": \"pune\", \"company\": \"telstra\", \"title\": \"It\"  }"
  };
    let result = {Count: 1};

    before(function beforeTest() {

      scanUserStub = sinon.stub(scanService, "scanUser");
      scanUserStub.callsFake(function (body) {
        expect(body["fullname"]).to.eq("Ankit");

        return Promise.resolve({Count: 1});
      });

      updateUserStub = sinon.stub(updateService, "updateUser");
      updateUserStub.callsFake(function (result, body) {
        expect(result.Count).to.eq(1);
        expect(body["fullname"]).to.eq("Ankit");

        return Promise.resolve(result);
      });
    });

    it('success', async function () {
      let event =  userParameter ;
      let context = {};

      let response = await create.create(event, context);

      expect(response.statusCode).to.eq(200);
      expect(response.body).to.eq('user already created with this mobile or email details are updated');
    });

    after(function afterTest() {
      scanUserStub.restore();
      updateUserStub.restore();
    });
  });

});