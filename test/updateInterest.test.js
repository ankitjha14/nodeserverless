const expect = require('chai').expect;
const sinon = require('sinon');



const updateInterestService = require("../services/updateInterestService");

let updateInterest = require("../api/updateInterest");

describe('updateInterest', function updateInterestTest() {
  let updateInterestStub;

  context('input ok', function () {
    let updateParameters = {body:"{\"name\":\"some param\"}"};
    let result = "result stub";

    before(function beforeTest() {

      updateInterestStub = sinon.stub(updateInterestService, "updateInterest");
      updateInterestStub.callsFake(function (body) {
        expect(body["name"]).to.eq("some param");

        return Promise.resolve(result);
      });
    });

    it('success', async function () {
      let event =  updateParameters ;
      let context = {};

      let response = await updateInterest.updateInterest(event, context);

      expect(response.statusCode).to.eq(200);
      expect(response.body).to.eq(JSON.stringify({
        message: JSON.stringify(result)
      }));
    });

    after(function afterTest() {
      updateInterestStub.restore();
    });
  });

  context('input missing', function () {
    let updateParameters = {body:""};

    it('failure', async function () {
      let event =  updateParameters;
      let context = {};

      let response = await updateInterest.updateInterest(event, context);

      expect(response.statusCode).to.eq(400);
      expect(response.body.message).to.eq('Invalid data');
    });

    after(function afterTest() {
      updateInterestStub.restore();
    });
  });

});