'use strict';

const updateInterestService = require('../services/updateInterestService');

module.exports.updateInterest = async (event, context) => {
  
  if(event.body==null || event.body==""){
    return {
      statusCode: 400,
      body: {
        message: "Invalid data"
      },
    };
  }

  const body = JSON.parse(event.body);

        let res = await updateInterestService.updateInterest(body)
        return {
          statusCode: 200,
          body: JSON.stringify({
            message: JSON.stringify(res)
          }),
        };
};