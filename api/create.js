'use strict';

const scanService = require('../services/scanService');
const updateService = require('../services/updateService');
const createService = require('../services/createService');

module.exports.create = async (event, context) => {
  const data = JSON.parse(event.body);

  if (!isValidData(data)) {
    return {
      statusCode: 400,
      body: "Invalid data",
    };
  }


  let scanResult = await scanService.scanUser(data)

 
  if(scanResult.Count>0){

    const res = await updateService.updateUser(scanResult, data);

    return {
      statusCode: 200,
      body: "user already created with this mobile or email details are updated",
    };
  }


  let res = await createService.createUser(data);
  
  return {
    statusCode: 200,
    body: "user sccessfully created",
  };
};

function isValidData(body){
    if(typeof body.fullname !== 'string' ||
    typeof body.email !== 'string' ||
    typeof body.mobile !== 'string' ||
    typeof body.address !== 'string' ||
    typeof body.company !== 'string' ||
    typeof body.title !== 'string')
    return false;

    return true;
}