'use strict';

const AWS = require('aws-sdk'); 

const dynamoDb = new AWS.DynamoDB.DocumentClient();

  async function scanUser(body) {
    const params = {
        TableName: "user",
        FilterExpression: 'mobile = :mobile or email = :email',
        ExpressionAttributeValues:{
            ":mobile": body.mobile,
            ":email": body.email
        }
      };

      let scanResult = await dynamoDb.scan(params).promise()

    return scanResult
  }
  
  module.exports = {
    scanUser
  };