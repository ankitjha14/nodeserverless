'use strict';

const AWS = require('aws-sdk'); 

const dynamoDb = new AWS.DynamoDB.DocumentClient();

  async function updateUser(scanResult, body) {
    const result = scanResult.Items[0];
            const updateparams = {
                TableName: "user",
                Key: {
                  user_id: result.user_id,
                },
                ExpressionAttributeValues: {
                  ':fullname': body.fullname,
                  ':address': body.address,
                  ':company': body.company,
                  ':title': body.title,
                },
                UpdateExpression: 'SET fullname = :fullname, address = :address, company = :company, title = :title',
                ReturnValues: 'ALL_NEW',
              };

              const res = await dynamoDb.update(updateparams).promise()

    return res
  }
  
  module.exports = {
    updateUser
  };