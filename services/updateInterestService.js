'use strict';

const AWS = require('aws-sdk'); 

const dynamoDb = new AWS.DynamoDB.DocumentClient();

  async function updateInterest(body) {
    const updateparams = {
      TableName: "user",
      Key: {
        user_id: body.user_id,
      },
      ExpressionAttributeNames: {
      '#interests' : 'interests',
      },
      ExpressionAttributeValues: {
          ':interests': body.interests,
      },
      UpdateExpression: 'SET #interests = :interests',
      ReturnValues: 'ALL_NEW',
    };

    let res = await dynamoDb.update(updateparams).promise()

    return res
  }
  
  module.exports = {
    updateInterest
  };