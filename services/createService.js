'use strict';

const uuid = require('uuid');
const AWS = require('aws-sdk'); 

const dynamoDb = new AWS.DynamoDB.DocumentClient();

  async function createUser(data) {
    const timestamp = new Date().getTime();
    
    const userParams = {
      TableName: "user",  
      Item: {
        user_id: uuid.v1(),
        fullname: data.fullname,
        email: data.email,
        mobile: data.mobile,
        address: data.address,
        company: data.company,
        title: data.title,
        createdAt: timestamp
      },
    };
  
    let res = await dynamoDb.put(userParams).promise()

    return res
  }
  
  module.exports = {
    createUser
  };