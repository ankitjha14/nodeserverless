Node Serverless

for using the application use these commands


Create User API

curl -X POST https://azo3rxq7e5.execute-api.ap-south-1.amazonaws.com/dev/create -H "Content-Type: text/plain" --data '{ "fullname": "Ankit", "email": "ankitjha14@gmail.com", "mobile": "8588984781", "address": "pune", "company": "telstra", "title": "It"  }'

Update User API

basically the same api as create user but when there is a match of email or mobile it updates the other data.

curl -X POST https://azo3rxq7e5.execute-api.ap-south-1.amazonaws.com/dev/create -H "Content-Type: text/plain" --data '{ "fullname": "Ankit", "email": "ankitjha14@gmail.com", "mobile": "8588984781", "address": "pune", "company": "telstra", "title": "It"  }'

Update User Interests API

for this you need user_id of a user.
curl -X POST https://azo3rxq7e5.execute-api.ap-south-1.amazonaws.com/dev/updateInterest -H "Content-Type: text/plain" --data '{ "interests": ["singing","pyaing"], "user_id": "56cc2c90-f2d4-11e8-8d38-1d7f927e44bf"  }'

to deploy the application please contact me or use your aws user account(put then in env variable of pipeline) with permission of lambda, cloudformation, s3


Implementation

I have used serverless framework with nodejs for it and mocha, sinon, chai for testing.

have used my personal AWS account.

I also have setup the pipepline for building and testing the code, there is also a seprate deployment plan. you can check it on below link
https://bitbucket.org/ankitjha14/nodeserverless/addon/pipelines/home

Assumption

- All the fields are mandatory.
- I can put some api gateway autorisation in my serverless.yml file but for ease of checking I leave them.


